<?php
/**
 * Copyright © 2016 Ihor Vansach (ihor@magefan.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */

namespace Magefan\Blog\Block;


use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;

/**
 * Abstract post мшуц block
 */
 class Home extends \Magento\Framework\View\Element\Template
{

    public $_storeManager;

    /**
     * @var \Magefan\Blog\Model\Post
     */
    protected $_post;

    /**
     * Page factory
     *
     * @var \Magefan\Blog\Model\PostFactory
     */
    protected $_postFactory;

    /**
     * @var Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var string
     */
    protected $_defaultPostInfoBlock = 'Magefan\Blog\Block\Post\Info';

    /**
     * @var \Magefan\Blog\Model\Url
     */
    protected $_url;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Cms\Model\Page $post
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Cms\Model\PageFactory $postFactory
     * @param \Magefan\Blog\Model\Url $url
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magefan\Blog\Model\Post $post,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
       // \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magefan\Blog\Model\PostFactory $postFactory,
        \Magefan\Blog\Model\Url $url,
		Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_post = $post;
        $this->_coreRegistry = $coreRegistry;
        $this->_storeManager = $context->getStoreManager();
        $this->_filterProvider = $filterProvider;
        $this->_postFactory = $postFactory;
        $this->_url = $url;
		$this->registry = $registry;
    }

    /**
     * Retrieve post instance
     *
     * @return \Magefan\Blog\Model\Post
     */
    public function getPost()
    {
        if (!$this->hasData('post')) {
            $this->setData('post',
                $this->_coreRegistry->registry('current_blog_post')
            );
        }
        return $this->getData('post');
    }

    /**
     * Retrieve post short content
     *
     * @return string
     */
    public function getShorContent($id)
    {
        $content = $this->getContent($id);
        $pageBraker = '<!-- pagebreak -->';

        $isMb = function_exists('mb_strpos');
        $p = $isMb ? strpos($content, $pageBraker) : mb_strpos($content, $pageBraker);

        if ($p) {
            $content = mb_substr($content, 0, $p);
        }

        return $content;
    }

    /**
     * Retrieve post content
     *
     * @return string
     */
    public function getContent($id)
    {
        $post = $this->_postFactory->create()->load($id);
        $key = 'filtered_content';
        if (!$post->hasData($key)) {
            $cotent = $this->_filterProvider->getPageFilter()->filter(
                $post->getContent()
            );
            $post->setData($key, $cotent);
        }
        return $post->getData($key);
    }

    /**
     * Retrieve post info html
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

    }
    
   public function getLatestPosts(){
		
		$var=$this->_postFactory->create()->getCollection()->addFieldToSelect('*')->addActiveFilter()->setOrder('update_time', 'DESC')->setPageSize(3);
		
	    return $var;
	
		}
		/**
     * Retrieve post url
     * @return string
     */
     
             
    
   
    public function getPostUrl($postId)
    {
        return  $this->_postFactory->create()->load($postId)->getPostUrl();
    }

	
}
