<?php

namespace  Tilray\ProductList\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /*
     * Magento\Customer\Model\Session $customerSession
     */
    protected  $customerSession;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}