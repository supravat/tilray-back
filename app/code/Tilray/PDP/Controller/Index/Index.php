<?php

namespace Tilray\PDP\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */

    protected $jsonData;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory; 
    
	/**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Customer\Model\Session $session,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonData = $jsonData;
        $this->_session = $session;
        $this->_productFactory = $productFactory;
        parent::__construct($context);
    }

    public function execute()
    {  
		
		
		if( !$this->_session->isLoggedIn()) {
            $response = [];
            /** Json Responce */
            $this->getResponse()->representJson(
                $this->jsonData->jsonEncode($response)
            );
            return ;
		}

		$productId = $this->getRequest()->getPostValue('productid');		
		$product =  $this->_productFactory->create()->load($productId);
		
		if($product) {
			$attributes = [
				'medical',
				'effects',
				'flavours'
			];
            
            foreach($attributes as $attribute) {
				$response[ ucfirst($attribute)]	= $product->getResource()->getAttribute($attribute)->getFrontend()->getValue($product);				
			}
		}		
		/** Json Responce */
		$this->getResponse()->representJson(
			$this->jsonData->jsonEncode($response)
		);
    } 
}
