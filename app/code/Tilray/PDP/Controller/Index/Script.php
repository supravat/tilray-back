<?php

namespace Tilray\PDP\Controller\Index;

use Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Customer\Model\Customer;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Tilray\Loyalty\Model\ResourceModel\Customer as LoyaltyCustomer;


class Script extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */

    protected $jsonData;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;


    protected $logger;
    protected $_orderCollectionFactory;
    protected $_loyaltyCustomer;
    private $customerRepository;
    private $_customers;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Customer\Model\Customer $customers,
        CustomerRepositoryInterface $customerRepository,
        OrderCollectionFactory $orderCollectionFactory,
        LoyaltyCustomer $loyaltyCustomer

    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonData = $jsonData;
        $this->_productFactory = $productFactory;
        $this->_customers = $customers;
        $this->customerRepository = $customerRepository;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_loyaltyCustomer = $loyaltyCustomer;
        parent::__construct($context);
    }


    /*
     * Loyalty - Senior One Year Anniversary
     *
     */


    public function execute()
    { echo '<pre>';
        $customerIds  = [];
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getSeniorWithOneCustomers();
        print_r($toBeUpdateCustomerIds);
        foreach($toBeUpdateCustomerIds as $customerId) {
            $order  = $this->_loyaltyCustomer->getFirstOrderDateById($customerId['entity_id']);
            print_r($order);
            if(isset($order[0]) && isset($order[0]['entity_id']) ) {
                $order = $order[0];
                $customerIds[] = $order['customer_id'];

            }
        } die;
        $this->_loyaltyCustomer->updateCustomersGroup($customerIds, 9 );
    }
    /*
     * NewPatientRegistration
     * 
     */
    
   /* public function execute()
    {
		$customerIds  = [];
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getAllOrderWithCustomer();
        //echo '<pre>'; print_r($toBeUpdateCustomerIds); 
        foreach($toBeUpdateCustomerIds as $order ) {
			if($order['count']>=3) {
				$customerIds[] = $order['customer_id'];
			}
		}
		$this->_loyaltyCustomer->updateCustomersGroup($customerIds, 7 );
    }*/
    
    /*
     * OneYearAnniversary
     * 
     */

}
