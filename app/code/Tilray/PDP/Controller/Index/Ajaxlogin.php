<?php

namespace Tilray\PDP\Controller\Index;

class Ajaxlogin extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */

    protected $jsonData;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;
    
    
	/**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Customer\Model\Session $session
    )
    {
        $this->jsonData = $jsonData;
        $this->_session = $session;
        parent::__construct($context);
    }

    public function execute()
    {            
		$response['flag'] = false;
		if( $this->_session->isLoggedIn()) {
		    $response['flag'] = true;
		    /** Json Responce */
		    $this->getResponse()->representJson(
		        $this->jsonData->jsonEncode($response)
		    );
		    return ;
		}
    } 
}
