/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Magento_SalesRule/summary/discount'
            },
            totals: quote.getTotals(),
            items : quote.getItems(),
            isDisplayed: function() {
                return this.isFullMode() && this.getPureValue() != 0;
            },
            getCouponCode: function() {
                if (!this.totals()) {
                    return null;
                }
                return this.totals()['coupon_code'];
            },
            getPureValue: function() {
                var savedPrice = 0;
                for(var i=0;i<this.items.length;i++){
                    console.log(this.items[i]);
                    var basePrice = this.items[i].base_price;
                    var bmsrp = this.items[i]['product'].price;
                    var splcprice = this.items[i]['product'].special_price;
                    var qty = this.items[i].qty;
                    if(bmsrp == 'undefined' || !bmsrp){
                        bmsrp = 0;
                    }
                    if(splcprice == 'undefined' || !splcprice){
                        splcprice = 0;
                    }
                    if(qty == 'undefined' || !qty){
                        qty = 1;
                    }
                    if(splcprice > 0 ) {
                        savedPrice += (parseFloat(bmsrp)- parseFloat(splcprice)) * qty;
                    }

                }
                var price = 0;
                if (this.totals() && this.totals().discount_amount) {
                    price += parseFloat(this.totals().discount_amount);
                }
                // if Catelog price rule applied
                var catalogDiscount = 0;
                if (bmsrp  >  basePrice) {
                    catalogDiscount += parseFloat(bmsrp) - parseFloat(basePrice) * qty;
                }
                return parseFloat(savedPrice) + parseFloat(Math.abs(price)) + parseFloat(Math.abs(catalogDiscount));
            },
            getValue: function() {
                return this.getFormattedPrice(this.getPureValue());
            }
        });
    }
);
