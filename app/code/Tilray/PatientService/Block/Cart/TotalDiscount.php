<?php
namespace Tilray\PatientService\Block\Cart;

use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Framework\Pricing\Helper\Data as PriceHelper;

    
class TotalDiscount extends \Magento\Framework\View\Element\Template {

	/** @var CheckoutSession */
    protected $checkoutSession;
    
    
	/** @var priceHelper */
    protected $priceHelper;
    
    	
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        CheckoutSession $checkoutSession,
        PriceHelper $priceHelper,
        array $data = []
    ) {
		$this->checkoutSession 	= $checkoutSession;
		$this->priceHelper 		= $priceHelper;
        parent::__construct($context, $data);
    }
    
    public function discountTotal() {
		
		/** @var \Magento\Quote\Model\Quote  */
		$quote = $this->checkoutSession->getQuote();
		
		
		$quote->getSubtotal();
		
		$discountTotal = 0;
		foreach ($quote->getAllItems() as $item){
			$discountTotal += $item->getDiscountAmount();
		}

		$formattedDiscountTotal = $this->priceHelper->currency(20, true, false);
		return  $formattedDiscountTotal;
		//return  sprintf(__('Your Total Savings on this order %s '), $quote->getSubtotal());
	}
}
?>
