<?php
namespace Tilray\PatientService\Block\Cart;

use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Framework\Pricing\Helper\Data as PriceHelper;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Catalog\Model\ProductRepository;


class Alert extends \Magento\Framework\View\Element\Template
{

    /** @var VAC_ALLOWANCE */
    CONST DISCOUNT_PRODUCT_TYPE = 30;


    /** @var CheckoutSession */
    protected $checkoutSession;


    /** @var priceHelper */
    protected $priceHelper;


    /** @var customerRepositoryInterface */
    protected $customerRepositoryInterface;

    /** @var productRepository */
    protected $productRepository;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        CheckoutSession $checkoutSession,
        PriceHelper $priceHelper,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ProductRepository $productRepository,
        array $data = []
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->priceHelper = $priceHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }


    public function getAlertMessage()
    {

        $blockId = '';

        /** @var \Magento\Quote\Model\Quote */
        $quote = $this->checkoutSession->getQuote();

        if (is_object($quote) && !is_null($quote->getCustomerId() )) {
            $customer = $this->customerRepositoryInterface->getById($quote->getCustomerId());
            $current_quote_qty = 0;

            foreach ($quote->getAllItems() as $item) {
                $_product = $this->productRepository->getById($item->getProductId());
                if ($_product->getType() == self::DISCOUNT_PRODUCT_TYPE) {
                    $current_quote_qty += $item->getQty();
                }
            }
            $prescription_allowance = is_object($customer->getCustomAttribute('prescription_allowance')) ? $customer->getCustomAttribute('prescription_allowance')->getValue() : 0 ;
            if ($current_quote_qty > $prescription_allowance) {
                return $blockId = 'prescription_allowance_exceed_message';
            }
            $vac_allowance = is_object($customer->getCustomAttribute('vac_allowance')) ? $customer->getCustomAttribute('vac_allowance')->getValue() : 0 ;
            if ($current_quote_qty > $vac_allowance) {
                if($current_quote_qty < $prescription_allowance) {
                    return $blockId = 'vac_allowance_exceed_message';
                }
            }

        }
        return $blockId;
    }
}

?>
