<?php
namespace Tilray\PatientService\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Framework\Message\ManagerInterface;

class LoadAfter implements ObserverInterface
{
	/** @var VAC_ALLOWANCE */
	CONST DISCOUNT_PRODUCT_TYPE = 30 ;
	
	
	/** @var CheckoutSession */
    protected $_checkoutSession;
	
	/** @var $_productRepository */
	protected $_productRepository;  
	
	/** @var $_messageManager */
	protected $_messageManager;
	
    /** @var customerRepositoryInterface */
    protected $_customerRepositoryInterface;
    	

    public function __construct(
		CheckoutSession $checkoutSession,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ProductRepository $productRepository,
		ManagerInterface $messageManager
		) 
		{
        $this->_checkoutSession = $checkoutSession;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_productRepository = $productRepository;
        $this->_messageManager = $messageManager;
    }

    public function execute(Observer $observer) {
		
		$message = false;
		
		/** @var \Magento\Quote\Model\Quote */
        $quote = $this->_checkoutSession->getQuote();

        // Applying coupon code
        $quote->setCouponCode('newcustomercode')->collectTotals()->save();

		if (is_object($quote) && !is_null($quote->getCustomerId() )) {
            $customer = $this->_customerRepositoryInterface->getById($quote->getCustomerId());
            $current_quote_qty = 0;

            foreach ($quote->getAllItems() as $item) {
                $_product = $this->_productRepository->getById($item->getProductId());
                if ($_product->getType() == self::DISCOUNT_PRODUCT_TYPE) {
                    $current_quote_qty += $item->getQty();
                }
            }
            $prescription_allowance = is_object($customer->getCustomAttribute('prescription_allowance')) ? $customer->getCustomAttribute('prescription_allowance')->getValue() : 0 ;
            
            if ($current_quote_qty > $prescription_allowance) {
                $quote->setHasError(true);
                //$this->_messageManager->addError(__("Modal ​window ​to ​alert ​customer ​when ​amount ​of ​cannabis ​product ​in ​cart exceeds ​prescription ​allocation"));
                return $this;
            }
            $vac_allowance = is_object($customer->getCustomAttribute('vac_allowance')) ? $customer->getCustomAttribute('vac_allowance')->getValue() : 0 ;
            if ($current_quote_qty > $vac_allowance) {
				if($current_quote_qty < $prescription_allowance) {
					//$this->_messageManager->addWarning(__("patient ​can ​make ​purchase ​but ​a ​page ​displays ​a warning ​message"));
				}
            }
        }
		return $this;
    }
}
