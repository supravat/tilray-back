<?php
namespace Tilray\PatientService\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Sales\Api\Data\OrderInterface;
use \Magento\Catalog\Model\ProductRepository;

class DecrementAllowance implements ObserverInterface
{
    /** @var VAC_ALLOWANCE */
    CONST DISCOUNT_PRODUCT_TYPE = 30 ;

    /** @var customerRepositoryInterface */
    protected $_customerRepositoryInterface;

    /** @var _orderInterface */
    protected $_orderInterface;

    /** @var productRepository */
    protected $_productRepository;

    public function __construct(
        CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        ProductRepository $productRepository)
    {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_orderInterface = $orderInterface;
        $this->_productRepository = $productRepository;
    }


    public function execute(Observer $observer)
    {

        try {
            $orderids = $observer->getEvent()->getOrderIds();
            foreach ($orderids as $orderid) {
                $order = $this->_orderInterface->load($orderid);
                if ($order->getId() && !$order->getCustomerIsGuest()) {
                    $current_quote_qty = 0;
                    foreach ($order->getAllItems() as $item) {
                        $_product = $this->_productRepository->getById($item->getProductId());
                        if ($_product->getType() == self::DISCOUNT_PRODUCT_TYPE) {
                            $current_quote_qty += $item->getQtyOrdered();
                            echo '<br/>';
                            var_dump($current_quote_qty);
                        }
                    }
                    $customer = $this->_customerRepositoryInterface->getById($order->getCustomerId());

                    $currentPrescriptionAllowance = $customer->getCustomAttribute('prescription_allowance')->getValue();
                    $currentVacAllowance = $customer->getCustomAttribute('vac_allowance')->getValue();


                    $updateVacUnit = (($currentVacAllowance - $current_quote_qty) >= 0)?   $currentVacAllowance - $current_quote_qty : 0 ;
                    $updatePrescriptionUnit = ($currentPrescriptionAllowance - $current_quote_qty >= 0)?   $currentPrescriptionAllowance - $current_quote_qty : 0 ;

                    $customer->setCustomAttribute('prescription_allowance', $updatePrescriptionUnit);
                    $customer->setCustomAttribute('vac_allowance', $updateVacUnit);


                    $customer = $this->_customerRepositoryInterface->save($customer);
                }
            }

        } catch (Exception $ex) {
            throw $ex->getMessage();
        }

    }
}
