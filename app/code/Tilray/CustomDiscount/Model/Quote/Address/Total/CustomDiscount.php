<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tilray\CustomDiscount\Model\Quote\Address\Total;

use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Catalog\Model\ProductRepository;

class CustomDiscount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
	/** @var VAC_ALLOWANCE */
	CONST DISCOUNT_PRODUCT_TYPE = 23 ;

	/** @var $_productRepository */
	protected $_productRepository; 
	
	/** @var customerRepositoryInterface */
    protected $_customerRepositoryInterface;		
    
    /**
    * @var \Magento\Framework\Pricing\PriceCurrencyInterface
    */
    protected $_priceCurrency;
 
    /**
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency [description]
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ProductRepository $productRepository
    ) {
        $this->_priceCurrency = $priceCurrency;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
		$this->_productRepository = $productRepository;
    }
 
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
        
    ) {

        parent::collect($quote, $shippingAssignment, $total);
        
		$i = 0 ;
		$quoteQty = 0;
		$calculateDiscount = 0;
		$allowenceQty = 0;
		$customer = $quote->getCustomer();
		$prescription_allowance = is_object($customer->getCustomAttribute('prescription_allowance')) ? $customer->getCustomAttribute('prescription_allowance')->getValue() : 0 ;
		$vac_allowance = is_object($customer->getCustomAttribute('vac_allowance')) ? $customer->getCustomAttribute('vac_allowance')->getValue() : 0 ;
		
		
		foreach ($quote->getAllItems() as $item) {
			$_product = $this->_productRepository->getById($item->getProductId());
			if ($_product->getType() == self::DISCOUNT_PRODUCT_TYPE) {
				$quoteQty += $item->getQty();
			}
		}
		
		

		if($quoteQty >= $vac_allowance) {
			$allowenceQty = $vac_allowance;
		} else if($quoteQty <= $vac_allowance) {
			$allowenceQty = $quoteQty ;
		}
		$discountrate = 8.5;
		$calculateDiscount = $discountrate * $allowenceQty; 
		

		$address = $shippingAssignment->getShipping()->getAddress();
		$label = 'vac_allowance';
		$subtotalAmount =$total->getSubtotal();
		//$TotalAmount= $subtotalAmount/10; //Set 10% discount
		$TotalAmount = $calculateDiscount; //Set our discount
		$discountAmount ="-".$TotalAmount;
		$appliedCartDiscount = 0;
		
		if($total->getDiscountDescription())
        {
            $appliedCartDiscount = $total->getDiscountAmount();
            $discountAmount = $total->getDiscountAmount()+$discountAmount;
            $label = $total->getDiscountDescription().', '.$label;
        }
		
		$quote->setCustomDiscount($discountAmount);

        $total->setDiscountDescription($label);
        $total->setDiscountAmount($discountAmount);
        $total->setBaseDiscountAmount($discountAmount);
        $total->setSubtotalWithDiscount($total->getSubtotal() + $discountAmount);
        $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() + $discountAmount);
        
        
        if(isset($appliedCartDiscount)) 
        {
			$total->addTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
			$total->addBaseTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
        }
        else
        {
            $total->addTotalAmount('customdiscount', $discountAmount);
			$total->addBaseTotalAmount('customdiscount', $discountAmount);
        }
        return $this;
    }
 
    /**
     * Assign subtotal amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param Address\Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
		$result = null;
		$amount = $total->getDiscountAmount();
		
		if ($amount != 0)
        {
            $description = $total->getDiscountDescription();
            $result = [
                'code' => $this->getCode(),
                'title' => strlen($description) ? __('Discount (%1)', $description) : __('Discount'),
                'value' => $amount
            ];
        }
        return $result;
        
    }
}
