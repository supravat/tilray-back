<?php

namespace Tilray\Loyalty\Cron;

use \Psr\Log\LoggerInterface;
use Tilray\Loyalty\Model\ResourceModel\Customer as LoyaltyCustomer;

class SeniorOneYearAnniversary
{

    /** @int CUSTOMER_GROUP_ID */
    CONST CUSTOMER_GROUP_ID = 9 ;

    protected $logger;
    protected $_loyaltyCustomer;



    public function __construct(
        LoggerInterface $logger,
        LoyaltyCustomer $loyaltyCustomer
    )
    {
        $this->logger = $logger;
        $this->_loyaltyCustomer = $loyaltyCustomer;
    }

    /*
     * Loyalty - Senior One Year Anniversary
     *
     */

    public function execute()
    {
        $customerIds  = [];
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getSeniorWithOneCustomers();
        foreach($toBeUpdateCustomerIds as $customerId) {
            $order  = $this->_loyaltyCustomer->getFirstOrderDateById($customerId['entity_id']);
            if(isset($order['entity_id'] )) {
                $customerIds[] = $customerId;
            }
        }
        $this->_loyaltyCustomer->updateCustomersGroup($customerIds,self::CUSTOMER_GROUP_ID);
    }

}
