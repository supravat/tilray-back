<?php

namespace Tilray\Loyalty\Cron;

use \Psr\Log\LoggerInterface;
use Tilray\Loyalty\Model\ResourceModel\Customer as LoyaltyCustomer;

class Patient
{
    /** @int NEW_PATIENT_GROUP */
    CONST NEW_PATIENT_CUSTOMER_GROUP = 7 ;


    /** @int ONE_YEAR_ANNIVERSARY_GROUP */
    CONST ONE_YEAR_ANNIVERSARY_GROUP = 8 ;


    /** @int SENIOR_ONE_YEAR_ANNIVERSARY_GROUP */
    CONST SENIOR_ONE_YEAR_ANNIVERSARY_GROUP = 9 ;


    protected $logger;
    protected $_loyaltyCustomer;

    public function __construct(
        LoggerInterface $logger,
        LoyaltyCustomer $loyaltyCustomer
    )
    {
        $this->logger = $logger;
        $this->_loyaltyCustomer = $loyaltyCustomer;
    }


    public function newPatientRegistration() {

        $customerIds  = [];
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getAllOrderWithCustomer();
        //echo '<pre>'; print_r($toBeUpdateCustomerIds);
        foreach($toBeUpdateCustomerIds as $order ) {
            if($order['count']>=3) {
                $customerIds[] = $order['customer_id'];
            }
        }
        $this->_loyaltyCustomer->updateCustomersGroup($customerIds, self::NEW_PATIENT_CUSTOMER_GROUP );
    }

    public function oneYearAnniversary() {
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getOneYearCustomers();
        $customerIds = [];
        if (count($toBeUpdateCustomerIds)) {
            foreach ($toBeUpdateCustomerIds as $id) {
                $customerIds[] = $id['entity_id'];
            }
        }
        $this->_loyaltyCustomer->updateCustomersGroup($customerIds, self::ONE_YEAR_ANNIVERSARY_GROUP);
    }

    public function seniorOneYearAnniversary() {
        //Get all customer's ids where
        $customerIds = [];
        $toBeUpdateCustomerIds = $this->_loyaltyCustomer->getSeniorWithOneCustomers();
        foreach($toBeUpdateCustomerIds as $customerId) {
            $order  = $this->_loyaltyCustomer->getFirstOrderDateById($customerId['entity_id']);
            if(isset($order[0]) && isset($order[0]['entity_id']) ) {
                $order = $order[0];
                $customerIds[] = $order['customer_id'];

            }
        }
        $this->_loyaltyCustomer->updateCustomersGroup($customerIds, self::SENIOR_ONE_YEAR_ANNIVERSARY_GROUP);
    }
}