<?php

namespace Tilray\Loyalty\Cron;

use \Psr\Log\LoggerInterface;
use \Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

class NewPatientRegistration
{

    protected $logger;
    protected $_orderCollectionFactory;
    private $customerRepository;
    private $_customers;

    public function __construct(
        LoggerInterface $logger,
        Customer $customers,
        CustomerRepositoryInterface $customerRepository,
        OrderCollectionFactory $orderCollectionFactory
    )
    {
        $this->logger = $logger;
        $this->_customers = $customers;
        $this->customerRepository = $customerRepository;
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * Write to system.log
     * @return void
     */

    public function execute()
    {
        try {
            $customers = $this->_customers->getCollection();
            foreach ($customers as $customer) {
                $orderCollection = $this->_orderCollectionFactory->create()->addAttributeToSelect('*');
                $orderCollection->addFieldToFilter('status', 'complete');
                $orderCollection->addFieldToFilter('subtotal', ['gteq' => 175]);
                $orderCollection->addFieldToFilter('customer_id', ['eg' => $customer->getId()]);
                //echo $orderCollection->getSelect()->__toString(). '<br/>';
                if (count($orderCollection) >= 3) { // Customer placed order 3 times over amount 175
                    $customerObj = $this->customerRepository->getById($customer->getId());
                    $customerObj->setGroupId(7);
                    $this->customerRepository->save($customerObj);
                }
            }
        } catch (Exception $e) {
            $logger->debug($e->getMessage());
        }

    }
}
