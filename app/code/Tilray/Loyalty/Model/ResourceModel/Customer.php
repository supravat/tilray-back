<?php

namespace Tilray\Loyalty\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\Timezone as TimeZone;
use Magento\Framework\Stdlib\DateTime\DateTime as DateTime;


/**
 * Customer resource model
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Customer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var DateTime
     */
    protected $date;
    protected $timezone;

    protected $timeZoneInterface;

    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var Sync
     */
    protected $syncResourceModel;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param DateTime $date
     * @param TimeZone $timezone
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     * @param Sync $syncResourceModel
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        DateTime $date,
        TimeZone $timezone,
        $connectionName = null
    )
    {
        parent::__construct($context, $connectionName);
        $this->date = $date;
        $this->timezone = $timezone;
    }

    public function getCustomers()
    {

        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('customer_entity'),
            ['entity_id']
        )->where(
            'is_active=?', 1
        )->where(
            'created_at > DATE_SUB(NOW(),INTERVAL 1 YEAR)'
        )->where(
            'group_id <> 7'
        );
        //echo $select->__toString(); die;
        return $connection->fetchAll($select);
    }

    public function getOneYearCustomers()
    {

        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('customer_entity'),
            ['entity_id']
        )->where(
            'is_active=?', 1
        )->where(
            'DATE(created_at) <= DATE(DATE_SUB(NOW(),INTERVAL 1 YEAR))'
        )->where(
            'group_id <> 8'
        );
        //echo $select->__toString(); die;
        return $connection->fetchAll($select);
    }

    public function updateCustomersGroup($ids = [], $groupId = 1)
    {

        if (!empty($ids)) {

            $connection = $this->getConnection();
            $bind = [
                'group_id' => $groupId
            ];

            $conditions = 'entity_id IN ( ' . implode(',', $ids) . ')';

            $connection->update(
                $this->getTable('customer_entity'), // table name
                $bind,                                // $bind  Column-value pairs
                $conditions                        // UPDATE WHERE clause(s).
            );
        }
    }

    public function getAllOrderWithCustomer()
    {

        $connection = $this->getConnection();

        $select = $connection->select()->from(
            'sales_order as order',
            ['entity_id', 'status', 'customer_id', 'subtotal', 'COUNT(*) as count']
        )->join(
            'customer_entity as customer',
            'customer.entity_id = order.entity_id',
            ['email', 'created_at', 'dob', 'is_active']
        )->where(
            'order.subtotal >= 175'
        )->where(
            'customer.is_active = ?', 1
        )->where(
            'customer.group_id <> 8'
        )->where(
            'order.status =?', 'complete'
        )->order(
            'order.created_at DESC'
        )->group('order.customer_id');

        //echo $select->__toString(); die;

        return $connection->fetchAll($select);
    }


    /*
     * Senior One Year Anniversary
     *
     */

    public function getSeniorWithOneCustomers()
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('customer_entity'),
            ['entity_id']
        )->where(
            'is_active=?', 1
        )->where(
            'DATE(dob) <= DATE(DATE_SUB(NOW(),INTERVAL 65 YEAR))'
        )->where(
            'DATE(created_at) <= DATE(DATE_SUB(NOW(),INTERVAL 1 YEAR))'
        )->where(
            'group_id <> 9'
        );

        return $connection->fetchAll($select);
    }


    public function getFirstOrderDateById($id = 0) {
        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('sales_order'),
            ['entity_id', 'status', 'customer_id', 'created_at']
        )->where(
            'customer_id =?', $id
        )->where(
            'created_at > DATE_SUB(NOW(),INTERVAL 1 YEAR)'
        )->where(
            'status =?', 'complete'
        )->order(
            'created_at DESC'
        )->limit(1);
        //echo '<BR/>'. $select->__toString(); die;
        return $connection->fetchAll($select);
    }

    protected function _construct()
    {
        $this->_init('customer_entity', 'id');
    }
}
