(function  () {
	require(['jquery'], function ($) {
		jQuery(document).ready(function(){

			// sign in account

			jQuery(".header-account").click(function(){
				jQuery(".header-account ul").toggle();
			});

			// video play

			$(".play-video").click(function(){
				$(".video-wrapper").show();
				$("iframe#tilray-v").attr("src", jQuery("iframe#tilray-v").attr("src").replace("autoplay=0", "autoplay=1"));
			});
			
			$(".close-video").click(function(){
				$(".video-wrapper").hide();
				$("iframe#tilray-v").attr("src", jQuery("iframe#tilray-v").attr("src").replace("autoplay=1", "autoplay=0"));
			});

			//adding shop by container in mobile

			if ( $(window).width() < 767) {
				$("#am_shopby_container").hide();	  
				$('#layered-filter-block .filter-title').click(function(){
					$('.filter-options').prepend($("#am_shopby_container"));
					$("#am_shopby_container").show();
					$(".filter-content .block-actions.filter-actions").appendTo("#narrow-by-list #am_shopby_container");
				});
			}


			$(".cross-acc").click(function(){

			    $(".cross-sell-products").toggle();

			});

			// increase and decrease quantity no product detail page

			var $plusbtn = $('.plus-value');

			var $minusbtn = $('.minus-value');

			$plusbtn.click(function(){

			  var $counter = $(this).closest('.qty-sep').find('.input-text.qty');

			  $counter.val( parseInt($counter.val()) + 1 ); // `parseInt` converts the `value` from a string to a number

			});

			$minusbtn.click(function(){

				var $counter = $(this).closest('.qty-sep').find('.input-text.qty');

		        var currentVal = parseInt($counter.val());

		        if (!isNaN(currentVal) && currentVal > 1) {

		            $counter.val(currentVal - 1);
		        }
		    });

		    // logged in product image below text

		    if ( $(window).width() < 767) {

		    	$(".img-text.prd-txt").appendTo(".varities");

			}
			
		}); // document.ready
		
		$(document).keyup(function(e) {

			27 === e.keyCode && ($(".video-wrapper").hide(), $("iframe#tilray-v").attr("src", jQuery("iframe#tilray-v").attr("src").replace("autoplay=1", "autoplay=0")), $(".header-account ul").hide())
		
		});

	});
})();

// (function  () {
// 	require(['jquery', 'js/owl.carousel.min'], function ($) {
// 		jQuery(document).ready(function(){

// 			//banner-section
// 			$('.tilray-banner').owlCarousel({
// 				loop:true,
// 				margin:10,
// 				responsiveClass:true,
// 				dots: true,
// 				nav: true,
// 				animateIn: 'fadeIn',
// 				animateOut: 'fadeOut',
// 				responsive:{
// 					0:{
// 						items:1,
// 						nav:false,
// 						dots : true
// 					},
// 					600:{
// 						items:1,
// 						nav:false,
// 						dots : true
// 					},
// 					1000:{
// 						items:1,
// 						nav:true,
// 						loop:false,
// 						dots : true
// 					},
// 					1100:{
// 						items:1,
// 						nav:true,
// 						mouseDrag: true,
// 						touchDrag: true,
// 						dots : true
// 					},
					
// 					1250:{						
// 						items:1,
// 						nav:true,
// 						mouseDrag: true,
// 						touchDrag: true,
// 						dots : true
// 					}
// 				}
// 			});
		
// 		});
// 	});
// })();
